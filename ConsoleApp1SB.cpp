﻿#include <iostream>
#include "Helpers.h"
#include <string>
#include <math.h> 
using namespace std;

class Animal
{
public:
	virtual void Voice() = 0;
};

class Dog : Animal
{
public:
	void Voice() override
	{
		cout << "Woof" << endl;
	}
};

class Cat : Animal
{
public:
	void Voice() override
	{
		cout << "Meow" << endl;
	}
};

class Cow : Animal
{
public:
	void Voice() override
	{
		cout << "Moo" << endl;
	}
};

int main()
{
	Dog* dog = new Dog();
	Cat* cat = new Cat();
	Cow* cow = new Cow();
	Animal* animals[] = { (Animal*)dog, (Animal*)cat, (Animal*)cow };
	for (auto animal : animals)
	{
		animal->Voice();
	}
	for (auto animal : animals)
	{
		delete animal;
		animal = nullptr;
	}
}